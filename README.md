# README #

1. This web is developed by HTML, CSS, AngularJS, Bootstrap.
2. I added some data into the JSON file, the total apartments is 15, and each page shows 5 apartments.
3. I added a attribute 'id' to each apartment in JSON file, so that when user jump to detail page, we can get the corresponding information.
4. Since AngularJS cannot read local JSON file without server, so you should first open your server like tomcat.
5. I deployed this web to the server, so you can visit www.wetest.com.s3-website-us-west-1.amazonaws.com
