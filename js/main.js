var app=angular.module('myApp', ['ngAnimate', 'ui.bootstrap','ui.router'])
// ui-router-init
app.config(['$urlRouterProvider','$stateProvider',function($urlRouterProvider,$stateProvider){
    $urlRouterProvider.otherwise('/home');
    $urlRouterProvider.when('', '/home');
    $urlRouterProvider.when('/', '/home');
    $stateProvider
        .state('home',{
            url:'/home',
            templateUrl:'./view/home.html',
            controller:'HomeController',
        })
        .state('detail',{
            url:'/detail/:id',
            templateUrl:'./view/detail.html',
            controller:'DetailController',
        })
}]);

app.controller('HomeController', ['$scope','$http',function ($scope, $http) {

  $scope.maxSize = 5;
  $scope.currentPage = 1;
  $scope.pageApartment = [];

  var orderByPopularity = "popularity";
  var orderByPrice = "price";  
  
  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;    
  };

  $scope.pageChanged = function() {
    $scope.changePageContent();    
  };

  $http.get("apartment.json")
    .success(function(response) {
        $scope.apartments = response.apartments; 
        $scope.totalItems = $scope.apartments.length;  
        $scope.numPages =   Math.ceil($scope.totalItems/$scope.maxSize);
        $scope.orderBy(orderByPopularity);
    });

  $scope.orderBy = function(name){

    $scope.currentPage = 1;    

    var property = document.getElementById(name);    
    if(name==orderByPopularity){
        document.getElementById(orderByPopularity).style.backgroundColor = "#e6b800";  
        document.getElementById(orderByPrice).style.backgroundColor = "#e6e6e6"; 

        $scope.apartments.sort(function(apartment1, apartment2){
            return apartment2.popularity-apartment1.popularity;
        })           
    }else if(name==orderByPrice){
        document.getElementById(orderByPrice).style.backgroundColor = "#e6b800";  
        document.getElementById(orderByPopularity).style.backgroundColor = "#e6e6e6";

        $scope.apartments.sort(function(apartment1, apartment2){
            return apartment2.price-apartment1.price;
        })            
    }  
    $scope.changePageContent();    
  }  

  $scope.changePageContent = function(){
    $scope.pageApartment = $scope.apartments.slice(($scope.currentPage-1)*5, $scope.currentPage*5);
  }
}]);

app.controller('DetailController',['$scope','$stateParams','$http',function($scope,$stateParams,$http){
    $scope.id = $stateParams.id;
    
    $http.get("apartment.json")
    .success(function(response) {
        var apartments = response.apartments; 
        var allAparts = apartments.filter(function(apt){
           return apt.id == $scope.id;
        })
        $scope.apartment = allAparts[0];
    });
}])


